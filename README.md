## PizzaBot

### Build

From the root of the project, run

`xcodebuild -project PizzaBot.xcodeproj`

### Run

Once built, from the root of the project, run


`build/Release/pizzabot`


Running with no arguments will provide the following help text


```
Usage: pizzabot "<instructions>" [animate]

instructions: <grid_size> <delivery_address> [<delivery_address> ...]
grid_size: e.g. 9x9
delivery_address: e.g. (9, 9)
animate: i.e. "animate"
```

### Example instructions

`build/Release/pizzabot "5x5 (0, 0) (1, 3) (4, 4) (4, 2) (4, 2) (0, 1) (3, 2) (2, 3) (4, 1)"`

To suppress warnings

`build/Release/pizzabot "5x5 (0, 0) (1, 3) (4, 4) (4, 2) (4, 2) (0, 1) (3, 2) (2, 3) (4, 1)" 2> /dev/null`

To animate the delivery

`build/Release/pizzabot "5x5 (0, 0) (1, 3) (4, 4) (4, 2) (4, 2) (0, 1) (3, 2) (2, 3) (4, 1)" animate`

![delivery](pizzabot.gif)