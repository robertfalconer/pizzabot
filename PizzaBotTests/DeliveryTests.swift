
import XCTest
@testable import PizzaBot

class DeliveryTests: XCTestCase {

    // Because delivery efficency is not currently a factor,
    // We'll not be testing certain delivery maps produce specific steps
    // We can, however, test some simple steps

    func testMovingNorth() {
        let map = DeliveryMap(with: MapSize(x:1, y:2), and: [DeliveryAddress(x:0, y:1)])
        let delivery = Delivery(for: map)
        let steps = delivery.deliveryStepsString()
        XCTAssertNotNil(steps.index(of: "N"))
        XCTAssertNotNil(steps.index(of: "D"))
    }

    func testMovingEast() {
        let map = DeliveryMap(with: MapSize(x:2, y:1), and: [DeliveryAddress(x:1, y:0)])
        let delivery = Delivery(for: map)
        let steps = delivery.deliveryStepsString()
        XCTAssertNotNil(steps.index(of: "E"))
        XCTAssertNotNil(steps.index(of: "D"))
    }

    func testMovingSouth() {
        let map = DeliveryMap(with: MapSize(x:2, y:2), and: [DeliveryAddress(x:0, y:1), DeliveryAddress(x:1, y:0)])
        let delivery = Delivery(for: map)
        let steps = delivery.deliveryStepsString()
        XCTAssertNotNil(steps.index(of: "N"))
        XCTAssertNotNil(steps.index(of: "E"))
        XCTAssertNotNil(steps.index(of: "S"))
        XCTAssertNotNil(steps.index(of: "D"))
    }
}
