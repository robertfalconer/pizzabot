
import XCTest
@testable import PizzaBot

class InstructionParserTests: XCTestCase {
    
    func testExpected() {
        let expectedString = "5x5 (1, 3) (4, 4)"
        var instructions: InstructionParser!
        XCTAssertNoThrow(instructions = try InstructionParser(with: expectedString), "Expected string should not fail")
        XCTAssert(instructions.mapSize == MapSize(x: 5, y: 5))
        XCTAssert(instructions.deliveryAddresses.count == 2)
        XCTAssert(instructions.deliveryAddresses[0] == DeliveryAddress(x: 1, y: 3))
        XCTAssert(instructions.deliveryAddresses[1] == DeliveryAddress(x: 4, y: 4))
    }
    
    func testInvalid() {
        let invalidString = "some random text that won't work"
        XCTAssertThrowsError(try InstructionParser(with: invalidString)) { error in
            XCTAssertEqual(error as? ArgumentError, ArgumentError.invalidMapSize)
        }
    }
    
    func testEmpty() {
        let emptyString = ""
        XCTAssertThrowsError(try InstructionParser(with: emptyString)) { error in
            XCTAssertEqual(error as? ArgumentError, ArgumentError.invalidMapSize)
        }
    }
    
    func testLargeMapSize() {
        let largeMapSizeString = "9999x9999 (1, 3) (4, 4)"
        var instructions: InstructionParser!
        XCTAssertNoThrow(instructions = try InstructionParser(with: largeMapSizeString), "Large map should not fail")
        XCTAssert(instructions.mapSize == MapSize(x: 9999, y: 9999))
        XCTAssert(instructions.deliveryAddresses.count == 2)
        XCTAssert(instructions.deliveryAddresses[0] == DeliveryAddress(x: 1, y: 3))
        XCTAssert(instructions.deliveryAddresses[1] == DeliveryAddress(x: 4, y: 4))
    }
    
    func testOutOfOrder() {
        let outOfOrderString = "(1, 3) (4, 4) 5x5"
        var instructions: InstructionParser!
        XCTAssertNoThrow(instructions = try InstructionParser(with: outOfOrderString), "Out of order should not fail")
        XCTAssert(instructions.mapSize == MapSize(x: 5, y: 5))
        XCTAssert(instructions.deliveryAddresses.count == 2)
        XCTAssert(instructions.deliveryAddresses[0] == DeliveryAddress(x: 1, y: 3))
        XCTAssert(instructions.deliveryAddresses[1] == DeliveryAddress(x: 4, y: 4))
    }
    
    func testMissingMapSize() {
        let missingMapSizeString = "(1, 3) (4, 4)"
        XCTAssertThrowsError(try InstructionParser(with: missingMapSizeString)) { error in
            XCTAssertEqual(error as? ArgumentError, ArgumentError.invalidMapSize)
        }
    }
    
    func testMissingAddresses() {
        let missingAddressString = "5x5"
        var instructions: InstructionParser!
        // No addressses to deliver to is an acceptabel state
        XCTAssertNoThrow(instructions = try InstructionParser(with: missingAddressString), "No addresses should not fail")
        XCTAssert(instructions.mapSize == MapSize(x: 5, y: 5))
        XCTAssert(instructions.deliveryAddresses.count == 0)
    }
    
    func testInvalidMapSize() {
        let invalidMapSizeString = "5y5 (1, 3) (4, 4)"
        XCTAssertThrowsError(try InstructionParser(with: invalidMapSizeString)) { error in
            XCTAssertEqual(error as? ArgumentError, ArgumentError.invalidMapSize)
        }
    }
    
    func testInvalidAddresss() {
        let invalidMapSizeString = "5x5 (x, y) (4, 4)"
        var instructions: InstructionParser!
        XCTAssertNoThrow(instructions = try InstructionParser(with: invalidMapSizeString), "Large address should not fail")
        XCTAssert(instructions.mapSize == MapSize(x: 5, y: 5))
        XCTAssert(instructions.deliveryAddresses.count == 1)
        XCTAssert(instructions.deliveryAddresses[0] == DeliveryAddress(x: 4, y: 4))

    }
    
    func testLargeDeliveryAddress() {
        let largeDeliveryAddressString = "9999x9999 (998, 998) (4, 4)"
        var instructions: InstructionParser!
        XCTAssertNoThrow(instructions = try InstructionParser(with: largeDeliveryAddressString), "Large address should not fail")
        XCTAssert(instructions.mapSize == MapSize(x: 9999, y: 9999))
        XCTAssert(instructions.deliveryAddresses.count == 2)
        XCTAssert(instructions.deliveryAddresses[0] == DeliveryAddress(x: 998, y: 998))
        XCTAssert(instructions.deliveryAddresses[1] == DeliveryAddress(x: 4, y: 4))
    }
    
    func testOutOfBoundsDeliveryAddress() {
        let outOfBoundsDeliveryAddressString = "999x999 (999, 999) (4, 4)"
        var instructions: InstructionParser!
        // This class is not concerned with correctness of the instructions, only the format
        XCTAssertNoThrow(instructions = try InstructionParser(with: outOfBoundsDeliveryAddressString), "Out of bounds address should not fail")
        XCTAssert(instructions.mapSize == MapSize(x: 999, y: 999))
        XCTAssert(instructions.deliveryAddresses.count == 2)
        XCTAssert(instructions.deliveryAddresses[0] == DeliveryAddress(x: 999, y: 999))
        XCTAssert(instructions.deliveryAddresses[1] == DeliveryAddress(x: 4, y: 4))
    }
    
    func testNoSpaceInDeliveryAddress() {
        let noSpaceInDeliveryAddressString = "5x5 (1,3) (4,4)"
        var instructions: InstructionParser!
        XCTAssertNoThrow(instructions = try InstructionParser(with: noSpaceInDeliveryAddressString), "Missing space should not fail")
        XCTAssert(instructions.mapSize == MapSize(x: 5, y: 5))
        XCTAssert(instructions.deliveryAddresses.count == 2)
        XCTAssert(instructions.deliveryAddresses[0] == DeliveryAddress(x: 1, y: 3))
        XCTAssert(instructions.deliveryAddresses[1] == DeliveryAddress(x: 4, y: 4))
    }
}
