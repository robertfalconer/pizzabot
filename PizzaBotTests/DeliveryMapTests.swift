
import XCTest
@testable import PizzaBot

class DeliveryMapTests: XCTestCase {

    func testExpected() {
        let expectedSize = MapSize(x: 5, y: 5)
        let expectedDeliveryAddresses = [DeliveryAddress(x: 1, y: 3), DeliveryAddress(x: 4, y: 4)]
        let map = DeliveryMap(with: expectedSize, and: expectedDeliveryAddresses)
        XCTAssert(map.origin == Location(x: 0, y: 0))
        XCTAssert(map.size == MapSize(x: 5, y: 5))
        XCTAssert(map.addresses.count == 2)
        XCTAssert(map.addresses[0] == DeliveryAddress(x: 1, y: 3))
        XCTAssert(map.addresses[1] == DeliveryAddress(x: 4, y: 4))
    }

    func testNoDeliveryAddresses() {
        let expectedSize = MapSize(x: 5, y: 5)
        let emptyDeliveryAddresses = [DeliveryAddress]()
        let map = DeliveryMap(with: expectedSize, and: emptyDeliveryAddresses)
        XCTAssert(map.origin == Location(x: 0, y: 0))
        XCTAssert(map.size == MapSize(x: 5, y: 5))
        XCTAssert(map.addresses.count == 0)
    }

    func testOutOFBoundsAddress() {
        let expectedSize = MapSize(x: 5, y: 5)
        let expectedDeliveryAddresses = [DeliveryAddress(x: 1, y: 3), DeliveryAddress(x: 5, y: 5)]
        let map = DeliveryMap(with: expectedSize, and: expectedDeliveryAddresses)
        XCTAssert(map.origin == Location(x: 0, y: 0))
        XCTAssert(map.size == MapSize(x: 5, y: 5))
        XCTAssert(map.addresses.count == 1)
        XCTAssert(map.addresses[0] == DeliveryAddress(x: 1, y: 3))

        //XCTAssert(prints a warning to stdErr) // Pretty sure we can't test that
    }

    func testDuplicateAddressesAddress() {
        let expectedSize = MapSize(x: 5, y: 5)
        let expectedDeliveryAddresses = [DeliveryAddress(x: 1, y: 3), DeliveryAddress(x: 4, y: 4), DeliveryAddress(x: 1, y: 3)]
        let map = DeliveryMap(with: expectedSize, and: expectedDeliveryAddresses)
        XCTAssert(map.origin == Location(x: 0, y: 0))
        XCTAssert(map.size == MapSize(x: 5, y: 5))
        XCTAssert(map.addresses.count == 2)
        XCTAssert(map.addresses[0] == DeliveryAddress(x: 1, y: 3))
        XCTAssert(map.addresses[1] == DeliveryAddress(x: 4, y: 4))

        //XCTAssert(prints a warning to stdErr) // Pretty sure we can't test that
    }
}
