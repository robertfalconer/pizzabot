
import XCTest
@testable import PizzaBot

class ArgumentsTests: XCTestCase {

    // No need to test the init(), as the compiler will take care of that
    // Only the conveinience Arguments.from(commandLineArguments:) needs tested

    func testExpected() {
        let expectedArgs = ["pizzabot", "5x5 (1, 3) (4, 4)"]
        var arguments: Arguments!
        XCTAssertNoThrow(arguments = try Arguments.from(commandLineArguments: expectedArgs), "Expected arguments should not fail")
        XCTAssert(arguments.commandName == "pizzabot")
        XCTAssert(arguments.instructions == "5x5 (1, 3) (4, 4)")
        XCTAssert(arguments.animate == false)
    }
    
    func testExpectedAnimated() {
        let expectedAnimatedArgs = ["pizzabot", "5x5 (1, 3) (4, 4)", "animate"]
        var arguments: Arguments!
        XCTAssertNoThrow(arguments = try Arguments.from(commandLineArguments: expectedAnimatedArgs), "Expected arguments should not fail")
        XCTAssert(arguments.commandName == "pizzabot")
        XCTAssert(arguments.instructions == "5x5 (1, 3) (4, 4)")
        XCTAssert(arguments.animate == true)
    }
    
    func testInvalidAnimated() {
        let unexpectedAnimatedArgs = ["pizzabot", "5x5 (1, 3) (4, 4)", "some_other_stuff"]
        var arguments: Arguments!
        XCTAssertNoThrow(arguments = try Arguments.from(commandLineArguments: unexpectedAnimatedArgs), "Expected instructions should not fail")
        XCTAssert(arguments.commandName == "pizzabot")
        XCTAssert(arguments.instructions == "5x5 (1, 3) (4, 4)")
        XCTAssert(arguments.animate == false)
    }
    
    func testNoArguments() {
        let noArgs: [String] = []
        XCTAssertThrowsError(try Arguments.from(commandLineArguments: noArgs)) { error in
            XCTAssertEqual(error as? ArgumentError, ArgumentError.missingArgument)
        }
    }
    
    func testNoInstructions() {
        let noInstructionsArgs = ["pizzabot"]
        XCTAssertThrowsError(try Arguments.from(commandLineArguments: noInstructionsArgs)) { error in
            XCTAssertEqual(error as? ArgumentError, ArgumentError.missingArgument)
        }
    }
}
