
class Delivery {
    enum DeliverySteps: String {
        case moveNorth = "N"
        case moveSouth = "S"
        case moveEast = "E"
        case deliverPizza = "D"
    }
    
    private let deliveryMap: DeliveryMap
    private var currentLocation: Location
    private (set) var deliverySteps: [DeliverySteps]
    private var deliveryAnimator: DeliveryAnimator?
    
    init(for deliveryMap: DeliveryMap, using animator: DeliveryAnimator? = nil) {
        self.deliveryMap = deliveryMap
        self.currentLocation = deliveryMap.origin
        self.deliverySteps = []
        self.deliveryAnimator = animator
        
        calulateDelivery()
    }
    
    func deliveryStepsString() -> String {
        let deliveryStepsStrings = deliverySteps.map { $0.rawValue }
        let deliveryString = deliveryStepsStrings.reduce("") { "\($0)\($1)" }
        return deliveryString
    }
    
    private func calulateDelivery() {
        currentLocation = deliveryMap.origin
        deliverySteps = []
        
        deliveryAnimator?.resetCanvas()
        
        while areUndeliveredAddresses() {
            if let deliveryAddress = deliveryAddress(for: currentLocation) {
                deliverPizza(to: deliveryAddress)
            } else if anyDeliveryAddressesNorth(of: currentLocation) {
                moveNorth()
            } else if anyDeliveryAddressesSouth(of: currentLocation) {
                moveSouth()
            } else {
                moveEast()
            }
        }
    }
    
    private func moveNorth() {
        currentLocation = Location(x: currentLocation.x, y: currentLocation.y + 1)
        deliverySteps.append(.moveNorth)
        deliveryAnimator?.draw(currentLocation, on: deliveryMap)
    }
    
    private func moveSouth() {
        currentLocation = Location(x: currentLocation.x, y: currentLocation.y - 1)
        deliverySteps.append(.moveSouth)
        deliveryAnimator?.draw(currentLocation, on: deliveryMap)
    }
    
    private func moveEast() {
        currentLocation = Location(x: currentLocation.x + 1, y: currentLocation.y)
        deliverySteps.append(.moveEast)
        deliveryAnimator?.draw(currentLocation, on: deliveryMap)
    }
    
    private func deliverPizza(to deliveryAddress: DeliveryAddress) {
        deliveryAddress.delivered = true
        deliverySteps.append(.deliverPizza)
        deliveryAnimator?.draw(currentLocation, on: deliveryMap)
    }
    
    private func deliveryAddress(for location: Location) -> DeliveryAddress? {
        return deliveryMap.addresses.filter({ $0 == location && $0.delivered == false }).first
    }
    
    private func anyDeliveryAddressesNorth(of location: Location) -> Bool {
        return deliveryMap.addresses.contains { $0.x == location.x && $0.y > location.y && $0.delivered == false }
    }
    
    private func anyDeliveryAddressesSouth(of location: Location) -> Bool {
        return deliveryMap.addresses.contains { $0.x == location.x && $0.y < location.y && $0.delivered == false }
    }
    
    private func anyDeliveryAddressesEast(of location: Location) -> Bool {
        return deliveryMap.addresses.contains { $0.y == location.y && $0.x > location.x && $0.delivered == false }
    }
    
    private func areUndeliveredAddresses() -> Bool {
        return deliveryMap.addresses.contains { $0.delivered == false }
    }
}
