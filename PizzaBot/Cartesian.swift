
protocol Cartesian {
    var x: Int { get }
    var y: Int { get }
}

func ==(lhs: Cartesian, rhs: Cartesian) -> Bool {
    return lhs.x == rhs.x && lhs.y == rhs.y
}
