import Foundation

class DeliveryAnimator {
    enum PointIcon: String {
        case standard = "."
        case current = "🚲"
        case undelivered = "🏠"
        case delivered = "🍕"
    }
    
    func resetCanvas() {
        print("\u{001B}[2J")
    }
    
    func draw(_ currentLocation: Location, on deliveryMap: DeliveryMap) {
        print("\u{001B}[0;0H")
        var line = ""
        for y in (0..<deliveryMap.size.y).reversed() {
            for x in 0..<deliveryMap.size.x {
                let drawingLocation = Location(x: x, y: y)
                var pointIcon: PointIcon = .standard
                
                if currentLocation == drawingLocation {
                    pointIcon = .current
                } else {
                    if let deliveryAddress = deliveryAddress(for: drawingLocation, on: deliveryMap) {
                        if deliveryAddress.delivered {
                            pointIcon = .delivered
                        } else {
                            pointIcon = .undelivered
                        }
                    }
                }
                
                if x > 0 {
                    line = "\(line)\t\(pointIcon.rawValue)"
                } else {
                    line = pointIcon.rawValue
                }
            }
            print(line)

            if y > 0 {
                print("\n\n")
            }
        }
        
        usleep(150000)
    }
    
    private func deliveryAddress(for location: Location, on deliveryMap: DeliveryMap) -> DeliveryAddress? {
        for deliveryAddress in deliveryMap.addresses {
            if location.x == deliveryAddress.x && location.y == deliveryAddress.y {
                return deliveryAddress
            }
        }
        
        return nil
    }
}
