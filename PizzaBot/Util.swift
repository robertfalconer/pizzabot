
import Foundation

class Util {
    static func error(_ error: String) {
        fputs("Error: \(error)\n", stderr)
    }

    static func warn(_ warning: String) {
        fputs("Warning: \(warning)\n", stderr)
    }

    static func printToStdErr(error: String, with prefix: String) {
        fputs("\(prefix)\(error)\n", stderr)
    }
}
