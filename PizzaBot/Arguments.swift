
struct Arguments {
    static let AnimateArgument = "animate"
    
    let commandName: String
    let instructions: String
    let animate: Bool
    
    init(commandName: String, instructions: String, animate: Bool) {
        self.commandName = commandName
        self.instructions = instructions
        self.animate = animate
    }
    
    static func from(commandLineArguments arguments: [String]) throws -> Arguments {
        switch arguments.count {
        case 0,1:
            throw ArgumentError.missingArgument
        case 2:
            return Arguments(commandName: arguments[0], instructions: arguments[1], animate: false)
        case 3:
            let animate = arguments[2] == AnimateArgument
            return Arguments(commandName: arguments[0], instructions: arguments[1], animate: animate)
         default:
            let animate = arguments[2] == AnimateArgument
            return Arguments(commandName: arguments[0], instructions: arguments[1], animate: animate)
        }
    }
}
