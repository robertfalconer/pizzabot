
enum ArgumentError: Error {
    case missingArgument
    case invalidMapSize
    case invalidDeliveryAddress
}
