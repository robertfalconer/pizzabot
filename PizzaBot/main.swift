import Foundation

do {
    let arguments = try Arguments.from(commandLineArguments: CommandLine.arguments)
    let animator = arguments.animate ? DeliveryAnimator() : nil
    let instructions = try InstructionParser(with: arguments.instructions)
    let map = DeliveryMap(with: instructions.mapSize, and: instructions.deliveryAddresses)
    let delivery = Delivery(for: map, using: animator)
    
    if animator == nil {
        print(delivery.deliveryStepsString())
    }
} catch ArgumentError.missingArgument {
    let usage = """

    Usage: pizzabot "<instructions>" [animate]
    
    instructions: <grid_size> <delivery_address> [<delivery_address> ...]
    grid_size: e.g. 9x9
    delivery_address: e.g. (9, 9)
    animate: i.e. "animate"


    """
    Util.printToStdErr(error: usage, with: "")
} catch {
    Util.error("\(error)")
    exit(EXIT_FAILURE)
}

exit(EXIT_SUCCESS)
