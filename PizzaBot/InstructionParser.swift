import Foundation

class InstructionParser {
    private let rawInstructions: String
    let mapSize: MapSize
    let deliveryAddresses: [DeliveryAddress]
    
    init(with instructions: String) throws {
        self.rawInstructions = instructions
        self.mapSize = try InstructionParser.parseMapSize(from: instructions)
        self.deliveryAddresses = try InstructionParser.parseDeliveryAddresses(from: instructions)
    }
    
    private static func parseMapSize(from instructions: String) throws -> MapSize {
        let mapSizePatternString = "(\\d+)x(\\d+)"
        let fullRange = NSRange(location: 0, length: instructions.utf16.count)
        guard let pattern = try? NSRegularExpression(pattern: mapSizePatternString, options: []),
            let firstMatch = pattern.firstMatch(in: instructions, options: [], range: fullRange) else {
                throw ArgumentError.invalidMapSize
        }
        
        let xRange = Range(firstMatch.range(at: 1), in: instructions)!
        let yRange = Range(firstMatch.range(at: 2), in: instructions)!
        
        guard let x = Int(instructions[xRange]),
            let y = Int(instructions[yRange]) else {
            throw ArgumentError.invalidMapSize
        }
        
        return MapSize(x: x, y: y)
    }
    
    private static func parseDeliveryAddresses(from instructions: String) throws -> [DeliveryAddress] {
        var deliveryAddresses = [DeliveryAddress]()

        let deliveryAddressPatternString = "\\((\\d+),\\s?(\\d+)\\)"
        let fullRange = NSRange(location: 0, length: instructions.utf16.count)

        guard let pattern = try? NSRegularExpression(pattern: deliveryAddressPatternString, options: []) else {
                throw ArgumentError.invalidMapSize
        }

        let matches = pattern.matches(in: instructions, options: [], range: fullRange)
        for match in matches {
            let xRange = Range(match.range(at: 1), in: instructions)!
            let yRange = Range(match.range(at: 2), in: instructions)!
            
            guard let x = Int(instructions[xRange]),
                let y = Int(instructions[yRange]) else {
                    throw ArgumentError.invalidDeliveryAddress
            }
            
            deliveryAddresses.append(DeliveryAddress(x: x, y: y))
        }
        
        return deliveryAddresses
    }
}
