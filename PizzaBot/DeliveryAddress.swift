
class DeliveryAddress: Cartesian {
    let x: Int
    let y: Int
    var delivered: Bool = false
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}
