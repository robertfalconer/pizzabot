
enum DeliveryMapError: Error {
    case addressOutOfBounds
    case addressAlreadyExists
}

class DeliveryMap {
    let origin = Location(x: 0, y: 0)
    let size: MapSize
    var addresses: [DeliveryAddress]
    
    init(with size: MapSize, and addresses: [DeliveryAddress]) {
        self.size = size
        self.addresses = DeliveryMap.validate(addresses, for: size)
    }
    
    private static func validate(_ addresses: [DeliveryAddress], for size: MapSize) -> [DeliveryAddress] {
        var validatedAddresses = [DeliveryAddress]()
        for address in addresses {
            if address.x >= size.x || address.y >= size.y {
                Util.warn("\(address) is out of bounds")
                continue
            }
            
            if validatedAddresses.contains(where: { $0 == address }) {
                Util.warn("\(address) is a duplicate")
                continue
            }
            
            validatedAddresses.append(address)
        }

        return validatedAddresses
    }
}
